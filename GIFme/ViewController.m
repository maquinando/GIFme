//
//  ViewController.m
//  GIFme
//
//  Created by Gustavo Peláez on 26/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import "ViewController.h"
#import "PreviewViewController.h"
#import "AFNetworking/AFNetworking.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@property (nonatomic, strong) AFHTTPRequestOperationManager *operationManager;
@property (nonatomic, strong) NSDictionary *settings;

@end

@implementation ViewController

BOOL _gridShow = NO;
BOOL _useTemporizer = NO;
id _resultImage = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"custom-settings.plist"];
    
    NSDictionary *baseSettings = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
    
    [settings addEntriesFromDictionary:baseSettings];
    
    self.settings = [NSDictionary dictionaryWithDictionary:settings];
    
    
    NSNumber *deviceId = [self.settings objectForKey:@"deviceId"];
    
//    if(!deviceId) {
        [self registerDevice];
//    }
    
    // Initiate device orientation
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    // Handle capture event
    [self.captureButton setUserInteractionEnabled:YES];    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePicture:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.captureButton addGestureRecognizer:singleTap];
    
    // start grid hidden;
    self.gridShow = NO;
    
    
    // Start InCam View
    [self.incam startWithDelegate:self];
    float shutterDelay = [[self.settings objectForKey:@"shutterDelay"] floatValue] / 1000;
    self.incam.animatedImagesDelay = shutterDelay;
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    
    [self.temporizerLayer setHidden:!_useTemporizer];
    self.temporizerLabel.layer.cornerRadius = 25;
}

- (void) showGrid:(BOOL)showGrid
{
    _gridShow = showGrid;
    [self.cameraGrid setHidden:!_gridShow];
    
}

- (BOOL) isGridShown
{
//    if (self.isGridShown == nil) {
//        self.isGridShown = NO;
//    }
    return _gridShow;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleGrid:(id)sender
{
    self.gridShow = !self.gridShow;
}
- (IBAction)toggleTemporizer:(id)sender
{
    _useTemporizer = YES;
    int temporizerCount = 5;
    [self.temporizerLayer setHidden:NO];
    
    for (int i = 0; i<temporizerCount; i++) {
        [NSTimer scheduledTimerWithTimeInterval:i target:self selector:@selector(countDown) userInfo:nil repeats:NO];
    }
    [NSTimer scheduledTimerWithTimeInterval:temporizerCount target:self selector:@selector(takePictures) userInfo:nil repeats:NO];
}

- (IBAction)flipCamera:(id)sender
{
    [self.incam flipCamera];
}

- (IBAction)takePicture:(id)sender
{
//    [self.incam takePicture];
    
    if(!_useTemporizer) {
        [self takePictures];
    }
}
- (void) takePictures
{
    [self.incam takePictures:[(NSString *)[self.settings objectForKey:@"gifImageCount"] intValue]];
    _useTemporizer = NO;
    [self.temporizerLayer setHidden:YES];
    [self.temporizerLabel setText:@"5"];
    
}
- (void) countDown {
    int count = [[self.temporizerLabel text] intValue];
    count--;
    [self.temporizerLabel setText:[NSString stringWithFormat:@"%i",count]];
    
}


- (IBAction)startTemporizer:(id)sender
{
    
}


#pragma mark - Incam Events
- (void)incamView:(UIView *)incamView captureOutput:(UIImage *)photo
{
    // Handle incam results
    _resultImage = photo;
    
    [self performSegueWithIdentifier: @"modalSegue" sender: self];
}
- (void)incamViewArray:(UIView *)incamView captureOutputArray:(NSArray *)photos
{
    // Handle incam results
    NSLog(@"Received %i picutes", [photos count]);
    _resultImage = photos;
    
    [self performSegueWithIdentifier: @"modalSegue" sender: self];
}


#pragma mark - Prepare for segue animation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([segue.identifier isEqualToString:@"pushSegue"]) {
        
        ICGNavigationController *fancyNavigationController = (ICGNavigationController *) self.navigationController;
        
        // Options: Slide, Scale, Layer, Flip
        NSString *animation = @"Slide";
        // Options Slide: @"From Left", @"From Right", @"From Top", @"From Bottom"
        // Options Scale: @"Fade In", @"Drop In"
        // Options Layer: @"Cover", @"Reveal"
        // Options Flip: @"From Left", @"From Right", @"From Top", @"From Bottom"
        NSString *type = @"From Left";
        NSInteger typeIndex = 0;
        
        
        fancyNavigationController.interactionEnabled = YES;
        NSString *className = [NSString stringWithFormat:@"ICG%@Animation", animation];
        id transitionInstance = [[NSClassFromString(className) alloc] init];
        fancyNavigationController.animationController = transitionInstance;
        fancyNavigationController.animationController.type = typeIndex;
        
    } else if ([segue.identifier isEqualToString:@"modalSegue"]) {
        PreviewViewController *viewController = segue.destinationViewController;
        
        
        
        ICGViewController *fancyViewController = (ICGViewController *) self;
        
        
        // Options: Slide, Scale, Layer, Flip
        NSString *animation = @"Scale";
        // Options Slide: @"From Left", @"From Right", @"From Top", @"From Bottom"
        // Options Scale: @"Fade In", @"Drop In"
        // Options Layer: @"Cover", @"Reveal"
        // Options Flip: @"From Left", @"From Right", @"From Top", @"From Bottom"
        NSString *type = @"Drop In";
        NSInteger typeIndex = 1;
        
        
        
        fancyViewController.interactionEnabled = YES;
        NSString *className = [NSString stringWithFormat:@"ICG%@Animation", animation];
        id transitionInstance = [[NSClassFromString(className) alloc] init];
        fancyViewController.animationController = transitionInstance;
        fancyViewController.animationController.type = typeIndex;
        
        if ([self respondsToSelector:@selector(setTransitioningDelegate:)]){
            viewController.transitioningDelegate = self.transitioningDelegate;  // this is important for the animation to work
        }
        
        if(_resultImage != nil) {
            if([_resultImage isKindOfClass:[NSArray class]]) {
                [viewController setOutputAnimatedImages:_resultImage];
            } else if([_resultImage isKindOfClass:[UIImage class]]) {
                viewController.outputImage = _resultImage;
            }
        }
        
    }
}

- (void) registerDevice
{
    @autoreleasepool {
        NSString *endpoint = [self.settings objectForKey:@"endpoint"];
        self.operationManager = [AFHTTPRequestOperationManager manager];
        self.operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
        
        NSString *uid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString *name = [[UIDevice currentDevice] name];
        NSString *systemName = [[UIDevice currentDevice] systemName];
        NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
        NSString *model = [[UIDevice currentDevice] model];
        
        NSDictionary *params = @{@"uid": uid,
                                 @"name": name,
                                 @"systemName": systemName,
                                 @"systemVersion": systemVersion,
                                 @"model": model};
        AFHTTPRequestOperation *operation = [self.operationManager POST:[endpoint stringByAppendingString:@"/device"] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"responseObject = %@", responseObject);
            NSDictionary *responseDict = (NSDictionary *) responseObject;
            NSDictionary *data = (NSDictionary *)[responseDict objectForKey:@"data"];
            NSString *deviceId =  [data objectForKey:@"deviceId"];
            
            NSMutableDictionary *mutableSettings = [NSMutableDictionary dictionary];
            [mutableSettings setObject:deviceId forKey:@"deviceId"];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

            NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"custom-settings.plist"];

            [mutableSettings writeToFile:plistPath atomically:YES];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error = %@", error);
        }];
    
        if (!operation) {
            NSLog(@"Creation of operation failed.");
        }
    }


}



@end
