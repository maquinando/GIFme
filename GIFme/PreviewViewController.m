//
//  PreviewViewController.m
//  GIFme
//
//  Created by Gustavo Peláez on 26/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import "PreviewViewController.h"
#import "UIImage+FiltrrCompositions.h"
#import "SVProgressHUD/SVProgressHUD.h"
#import "AFNetworking/AFNetworking.h"
#import "UIImage+fixRotation.h"
#import "UIImage+Scale.h"
@interface PreviewViewController ()


@property (weak,nonatomic) UIImage *originalImage;
@property (strong,nonatomic) NSArray<UIImage *> *originalArray;

@property (nonatomic, strong) AFHTTPRequestOperationManager *operationManager;


@property (nonatomic, strong) NSDictionary *settings;
@property (nonatomic) BOOL acceptNetworks;
@property (nonatomic) BOOL acceptEvent;

@end

@implementation PreviewViewController


BOOL _doAnimate = NO;


-(void)customAlertOK
{
    self.acceptEvent = _customAlert.chkPublicImage.checked;
    self.acceptNetworks = _customAlert.chkSendPromoInfo.checked;
    [self upload];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)customAlertCancel
{
//    [self upload];
//    [self dismissViewControllerAnimated:YES completion:nil];
    [_customAlert dismissViewControllerAnimated:YES completion:nil];
}


- (void)awakeFromNib
{
    [super awakeFromNib];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"custom-settings.plist"];
    
    NSDictionary *baseSettings = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"]];
    
    [settings addEntriesFromDictionary:baseSettings];
    
    self.settings = [NSDictionary dictionaryWithDictionary:settings];
    
    // Set preview effects
    self.noFilter.effect = @"";
    self.redFilter.effect = @"red";
    self.blueFilter.effect = @"blue";

    
    // Do any additional setup after loading the view.
    [self.previewImageView setDelegate:self];
    
    float delay = [[self.settings objectForKey:@"gifImagesLag"] floatValue] / 1000;
    self.previewImageView.animatedImageDelay = delay;
    if (_outputImage) {
        [self previewImage:_outputImage];
    } else if(self.originalArray) {
        [self previewAnimatedImage:self.originalArray];
    }
    
    self.acceptEvent = YES;
    self.acceptNetworks = YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Custom

- (void) setOutputAnimatedImages: (NSArray *)imagesArray
{
    _doAnimate = YES;
    for (int i=0; i<imagesArray.count; i++) {
        UIImage *img = [imagesArray objectAtIndex:i];
        
        CGSize targetSize = img.size;
        float width = targetSize.width*0.5;
        float height = targetSize.height*0.5;
        UIImage *newImage = [UIImage imageWithImage:img scaledToSize:CGSizeMake(width, height)];
        
        [self pushOriginalImage:newImage];
    }
    for (int i=self.originalArray.count-2; i>0; i--) {
        UIImage *img = [imagesArray objectAtIndex:i];
        
        [self pushOriginalImage:img];
    }
}
- (void) pushOriginalImage:(UIImage *)image {
    NSMutableArray *aux = [self.originalArray mutableCopy];
    if (!aux) {
        aux = [[NSMutableArray alloc] init];
    }
    
    image = [image fixRotation];
    
    [aux addObject:image];
    self.originalArray = aux;
}



- (void) setOutputImage:(UIImage *)outputImage
{
    _outputImage = outputImage;
}

- (void) previewAnimatedImage: (NSArray<UIImage *> *) originalArray
{
    [self.previewImageView setAnimationImages:originalArray];
//    [self.previewImageView setTransform:CGAffineTransformMakeRotation(M_PI_2)];
    float delay = [[self.settings objectForKey:@"gifImagesLag"] floatValue] / 1000;
    
    [self.previewImageView setAnimationDuration:delay * [originalArray count]];
    [self.previewImageView setAnimationRepeatCount:0];
    [self.previewImageView startAnimating];
    
    
    CGSize size = (CGSize){79, 107};
    UIImage *preview = [originalArray firstObject];
    if(preview){
        preview = [preview imageByScalingProportionallyToSize:size];
        
        [self.noFilter setImage:preview forState:UIControlStateNormal];
        [self.redFilter setImage:preview forState:UIControlStateNormal];
        [self.blueFilter setImage:preview forState:UIControlStateNormal];
    }
}
- (void) previewImage: (UIImage *) outputImage
{
    [self.previewImageView setImage:outputImage];
    
    CGSize size = (CGSize){79, 107};
    UIImage *preview = [outputImage scaleToSize:size];
    [self.noFilter setImage:preview forState:UIControlStateNormal];
    [self.redFilter setImage:preview forState:UIControlStateNormal];
    [self.blueFilter setImage:preview forState:UIControlStateNormal];
}
- (IBAction)applyFilter:(id)sender
{
    UIButtonBox *box = (UIButtonBox *)sender;
    NSString *effect = box.effect;
    
    self.previewImageView.effect = effect;
    
}

- (UIImage *) imageApplyEffect:(UIImage *)image effect:(NSString *)effect
{
    SEL selector = NSSelectorFromString(effect);
    if(![image respondsToSelector:selector]) {
        return image;
    }
    return [image performSelector:selector];
}

- (IBAction)backButtonTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)sendButtonTapped:(id)sender
{
    
    
    
//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Acepta los términos y condiciones?"
//                                                                   message:@"La imagen será enviada a nuestro servidor para ser publicada en la campaña de lanzamiento."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* dismiss = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
//            [self dismissViewControllerAnimated:YES completion:nil];
//    }];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Si" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//        
//    }];
//    [alert addAction:dismiss];
//    [alert addAction:defaultAction];
//    
//    [self presentViewController:alert animated:YES completion:nil];
//    
    
    
    
    _customAlert = [[CustomAlertViewController alloc] init];
    [_customAlert setDelegate:self];
    [_customAlert removeCancelButton:NO];
    [_customAlert showCustomAlertInView:self.view
                            withMessage:@"Autorizas utilizar el GIF EN?:"];
    
}

- (void) upload {
    
    [SVProgressHUD showInfoWithStatus:@"Preparando archivo .gif"];
    [self.previewImageView prepareGIFImage];
}



- (void)myImageView:(UIImageView *)imageView captureGIFImageData:(NSData *)photo
{
    
}
- (void)myImageView:(UIImageView *)imageView captureGIFImageURL:(NSURL *)file
{

    @autoreleasepool {
        
    
        NSString *endpoint = [self.settings objectForKey:@"endpoint"];
        NSString *deviceId = [self.settings objectForKey:@"deviceId"];
        
        if (!deviceId) {
            deviceId = @"1";
        }
        NSString *effect = self.previewImageView.effect;
        if(!effect) effect = @"";
        NSDictionary *params = @{@"deviceId": deviceId,
                                 @"filter": effect,
                                 @"accept_terms": [NSString stringWithFormat:@"%d",YES],
                                 @"publish_event": [NSString stringWithFormat:@"%d", self.acceptEvent],
                                 @"publish_networks": [NSString stringWithFormat:@"%d", self.acceptNetworks],};
    


        [SVProgressHUD showInfoWithStatus:@"Subiendo imágenes al servidor"];
        self.operationManager = [AFHTTPRequestOperationManager manager];
        self.operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
        self.operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        [self.operationManager.requestSerializer setTimeoutInterval:600];
        
        AFHTTPRequestOperation *operation = [self.operationManager POST:[endpoint stringByAppendingString:@"/upload"] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            NSError *error;
            if (![formData appendPartWithFileURL:file name:@"image" fileName:[file lastPathComponent] mimeType:@"image/gif" error:&error]) {
                NSLog(@"error appending part: %@", error);
            }
        } success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([responseObject isKindOfClass:[NSArray class]]) {
                NSArray *responseArray = responseObject;
                /* do something with responseArray */
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                NSDictionary *responseDict = responseObject;
                /* do something with responseDict */
            } else {
                NSString* responseStr = [NSString stringWithUTF8String:[responseObject bytes]];
            }
            NSDictionary *response = (NSDictionary *)responseObject;
            NSLog(@"responseObject = %@", responseObject);
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error = %@", error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        }];
    
        if (!operation) {
            NSLog(@"Creation of operation failed.");
        }
    }
    
}
- (void)myImageView:(UIImageView *)imageView captureGIFImage:(UIImage *)image
{
    
}
- (void)myImageView:(UIImageView *)imageView captureFilteredImage:(NSArray *)photos
{
    
}


@end
