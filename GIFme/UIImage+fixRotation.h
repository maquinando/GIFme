//
//  UIImage+fixRotation.h
//  GIFme
//
//  Created by Gustavo Peláez on 1/12/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixRotation)

- (UIImage *)fixRotation;
- (UIImage *)blendImage: (UIImage *)image;

@end
