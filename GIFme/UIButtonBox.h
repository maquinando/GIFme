//
//  UIButtonBox.h
//  GIFme
//
//  Created by Gustavo Peláez on 27/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol KIIncamDelegate <NSObject>

@required

- (void)apply:(UIView *)incamView captureOutput:(UIImage *)photo;

@end


@interface UIButtonBox : UIButton

@property NSString *effect;

@end
