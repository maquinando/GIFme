//
//  CustomAlertViewController.h
//  GIFme
//
//  Created by Gustavo Peláez on 6/12/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICheckBox.h"

#define OK_BUTTON_TAG       888
#define CANCEL_BUTTON_TAG   999
#define ANIMATION_DURATION  0.25

@protocol CustomAlertViewControllerDelegate
    -(void)customAlertOK;
    -(void)customAlertCancel;
@end

@interface CustomAlertViewController : UIViewController

@property (nonatomic, retain) id<CustomAlertViewControllerDelegate> delegate;


- (IBAction)btnOkayTap:(id)sender;
- (IBAction)btnCancelTap:(id)sender;
@property (weak, nonatomic) IBOutlet UICheckBox *chkSendPromoInfo;
@property (weak, nonatomic) IBOutlet UICheckBox *chkPublicImage;
@property (weak, nonatomic) IBOutlet UIView *viewMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnOK;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *btnCancel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *flexSpace;



-(void)showCustomAlertInView:(UIView *)targetView withMessage:(NSString *)message;
-(void)removeCustomAlertFromView;
-(void)removeCustomAlertFromViewInstantly;
-(void)removeOkayButton:(BOOL)shouldRemove;
-(void)removeCancelButton:(BOOL)shouldRemove;
-(BOOL)isOkayButtonRemoved;
-(BOOL)isCancelButtonRemoved;

@end
