//
//  UICheckBox.h
//  GIFme
//
//  Created by Gustavo Peláez on 6/12/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>


@interface UICheckBox : UIButton {
    BOOL checked;
}

@property (nonatomic, assign) BOOL checked;

-(IBAction)checkBoxClicked;



@end
