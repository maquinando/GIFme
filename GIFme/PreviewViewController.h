//
//  PreviewViewController.h
//  GIFme
//
//  Created by Gustavo Peláez on 26/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICGNavigationController.h"
#import "ICGViewController.h"
#import "UIImage+FiltrrCompositions.h"
#import "UIButtonBox.h"
#import "MyImageView.h"
#import "CustomAlertViewController.h"


@interface PreviewViewController : ICGViewController <MyImageViewDelegate, CustomAlertViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MyImageView *previewImageView;

@property (weak, nonatomic) IBOutlet UIButtonBox *noFilter;
@property (weak, nonatomic) IBOutlet UIButtonBox *redFilter;
@property (weak, nonatomic) IBOutlet UIButtonBox *blueFilter;

@property (weak, nonatomic) UIImage *outputImage;

@property (nonatomic, strong) CustomAlertViewController *customAlert;


- (void) setOutputAnimatedImages: (NSArray *) imagesArray;
- (IBAction)applyFilter:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)sendButtonTapped:(id)sender;

@end
