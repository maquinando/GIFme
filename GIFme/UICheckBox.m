#import "UICheckBox.h"


@implementation UICheckBox
@synthesize checked;


-(id)initWithFrame:(CGRect)frame{
    
    if(self == [super initWithFrame:frame]){
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
        NSString *imgPath = @"checkbox_not_ticked.png";
        if(self.checked) {
            imgPath = @"checkbox_ticked.png";
        }
        [self setImage:[UIImage imageNamed:imgPath] forState:UIControlStateNormal];
        
        [self addTarget:self action:@selector(checkBoxClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return self;
    
}

-(IBAction)checkBoxClicked{
    
    if(self.checked == NO)
    {
        self.checked = YES;
        [self setImage:[UIImage imageNamed:@"checkbox_ticked.png"] forState:UIControlStateNormal];
    }
    else{
        self.checked = NO;
        [self setImage:[UIImage imageNamed:@"checkbox_not_ticked.png"] forState:UIControlStateNormal];
    }
    
}

@end