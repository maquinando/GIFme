//
//  MyImageView.h
//  GIFme
//
//  Created by Gustavo Peláez on 27/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyImageViewDelegate <NSObject>

@required

- (void)myImageView:(UIImageView *)imageView captureGIFImageData:(NSData *)photo;
- (void)myImageView:(UIImageView *)imageView captureGIFImageURL:(NSURL *)file;
- (void)myImageView:(UIImageView *)imageView captureGIFImage:(UIImage *)image;
- (void)myImageView:(UIImageView *)imageView captureFilteredImage:(NSArray *)photos;

@end

@interface MyImageView : UIImageView

@property (strong, nonatomic) NSString *effect;
@property (strong, nonatomic) CAGradientLayer *effectLayer;
@property (strong, nonatomic) id<MyImageViewDelegate> delegate;

@property (nonatomic) float animatedImageDelay;

- (void)setDelegate:(id<MyImageViewDelegate>)delegate;
- (BOOL) prepareGIFImage;

@end
