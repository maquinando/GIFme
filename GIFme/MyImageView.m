//
//  MyImageView.m
//  GIFme
//
//  Created by Gustavo Peláez on 27/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import "MyImageView.h"
#import "UIImage+FiltrrCompositions.h"
#import "UIImage+animatedGIF.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+fixRotation.h"
#import "UIImage+FiltrrCompositions.h"

@interface MyImageView()



@end
@implementation MyImageView





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void) awakeFromNib
{
    [super awakeFromNib];
}
//- (void) setImage:(UIImage *)image
//{
//    if (self.effect){
//        SEL selector = NSSelectorFromString(self.effect);
//        if([image respondsToSelector:selector]) {
//            image =  [image performSelector:selector];            
//        }
//    }
//    
//    [super setImage:image];
//    [self setAnimationRepeatCount:0];
//    [self startAnimating];
//}
//- (void) setAnimationImages:(NSArray<UIImage *> *)animationImages
//{
//    if (self.effect){
//        SEL selector = NSSelectorFromString(self.effect);
//        if([[animationImages firstObject] respondsToSelector:selector]) {
//            
//            NSMutableArray *resultImages = [NSMutableArray array];
//            
//            for (int i=0; i<[animationImages count]; i++) {
//                UIImage *p = [(UIImage *)[animationImages objectAtIndex:i] performSelector:selector];
//                [resultImages addObject:p];
//            }
//            animationImages  = [NSArray arrayWithArray:resultImages];
//        }
//    }
//    
//    
//    [super setAnimationImages:animationImages];
//    [self setAnimationDuration:3.0];
//    [self setAnimationRepeatCount:0];
//    [self startAnimating];
//}

- (void) setAnimationImages:(NSArray<UIImage *> *)animationImages {
    [super setAnimationImages:animationImages];
    [self setAnimationDuration:self.animatedImageDelay*animationImages.count];
    [self setAnimationRepeatCount:0];
    [self startAnimating];
}
- (void) setEffect:(NSString *)effect
{
    _effect = effect;
    [self applyFilter:effect];
}

- (void) applyFilter:(NSString *) filter
{
    
    UIColor *theColor;
    if([filter isEqualToString:@"red"]){
        theColor = [UIColor redColor];
    } else if([filter isEqualToString:@"blue"]){
        theColor = [UIColor blueColor];
    } else {
        theColor = [UIColor clearColor];
        if(_effectLayer) {
            [_effectLayer removeFromSuperlayer];
        }
        return;
    }
    
    

    if (!_effectLayer) {
        
        _effectLayer = [CAGradientLayer layer];
        //the gradient layer must be positioned at the origin of the view
//        CGRect gradientFrame = self.frame;
//        gradientFrame.origin.x = -20;
//        gradientFrame.origin.y = 0;
        _effectLayer.frame = self.bounds;
        
        
    } else {
        [_effectLayer removeFromSuperlayer];
    }
    
    
    
    //build the colors array for the gradient
    NSArray *colors = [NSArray arrayWithObjects:
                       (id)[[theColor colorWithAlphaComponent:0.25f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.25f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.25f] CGColor],
                       (id)[[theColor colorWithAlphaComponent:0.25f] CGColor],
                       nil];
    
    //reverse the color array if needed
//    if(transparentToOpaque)
//    {
//        colors = [[colors reverseObjectEnumerator] allObjects];
//    }
    
    //apply the colors and the gradient to the view
    _effectLayer.colors = colors;
    
    [self.layer insertSublayer:_effectLayer atIndex:0];
}

- (BOOL) prepareGIFImage {
    if (![self isAnimating]) return NO;
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    NSArray *images = [self animationImages];
    
    NSDictionary *fileProperties = @{
                                     (__bridge id)kCGImagePropertyGIFDictionary: @{
                                             (__bridge id)kCGImagePropertyGIFLoopCount: @0, // 0 means loop forever
                                             
                                             (__bridge id)kCGImageDestinationLossyCompressionQuality: [[NSNumber alloc] initWithFloat:0.5], // a float
                                             }
                                     };
    NSDictionary *frameProperties = @{
                             (__bridge id)kCGImagePropertyGIFDictionary: @{
                                     (__bridge id)kCGImagePropertyGIFDelayTime: [[NSNumber alloc] initWithFloat:self.animatedImageDelay], // a float (not double!) in seconds, rounded to centiseconds in the GIF data
                                     
                                     (__bridge id)kCGImageDestinationLossyCompressionQuality: [[NSNumber alloc] initWithFloat:0.5], // a float
                                     },
                             };

    
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
    
    NSURL *fileURL = [documentsDirectoryURL URLByAppendingPathComponent:@"animated.gif"];
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((__bridge CFURLRef)fileURL, kUTTypeGIF, images.count, NULL);
    CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)fileProperties);

    
    for(int i=0; i<images.count; i++) {
        UIImage *img = [images objectAtIndex:i];
//        img = [img  fixRotation];
        if(self.effect && ![@"" isEqualToString:self.effect]) {
            SEL selector = NSSelectorFromString(self.effect);
            if([img respondsToSelector:selector]) {
                img = [img performSelector:selector];
            }
        }

        CGImageDestinationAddImage(destination, img.CGImage, (__bridge CFDictionaryRef) frameProperties);
    }
    if (!CGImageDestinationFinalize(destination)) {
        NSLog(@"failed to finalize image destination");
    }
    CFRelease(destination);
    NSLog(@"url=%@", fileURL);
//    UIImage *gif = [UIImage animatedImageWithAnimatedGIFURL:fileURL];
    [self.delegate myImageView:self captureGIFImageURL:fileURL];
        
//    });

    return YES;
}

- (void) setDelegate:(id<MyImageViewDelegate>)delegate
{
    _delegate = delegate;
}


- (float) animatedImageDelay
{
    if(!_animatedImageDelay) {
        _animatedImageDelay = 0.15f;
    }
    return _animatedImageDelay;
}


@end
