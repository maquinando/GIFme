#import <UIKit/UIKit.h>

@protocol KIIncamDelegate <NSObject>

@required

- (void)incamView:(UIView *)incamView captureOutput:(UIImage *)photo;
- (void)incamViewArray:(UIView *)incamView captureOutputArray:(NSArray *)photos;

@end

@interface KIIncam : UIView <UIGestureRecognizerDelegate>

@property (nonatomic) int animatedImagesCount;
@property (nonatomic) float animatedImagesDelay;

- (id)initWithCoder:(NSCoder *)aDecoder;
- (id)initWithFrame:(CGRect)frame;

- (void)startWithDelegate:(id<KIIncamDelegate>)delegate;
- (void)takePicture;
- (void)takePictures:(int) count;
- (void)flipCamera;

@end
