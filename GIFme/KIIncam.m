#import "KIIncam.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+Scale.h"

@interface KIIncam ()

@property AVCaptureSession *session;
@property AVCaptureStillImageOutput *imageOutput;
@property AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, weak) id<KIIncamDelegate> delegate;
@property (nonatomic) AVCaptureDevicePosition position;

@property (weak,nonatomic) NSArray *gifImagesArray;

@end

@implementation KIIncam

int _animatedImagesCount;
int _animatedImagesDelay;
int _animatedImagesIterator;
AVCaptureDevicePosition _position;

NSArray *_gifImagesArray;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeCapture];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeCapture];
    }
    return self;
}

- (void)startWithDelegate:(id<KIIncamDelegate>)delegate {
    
    self.delegate = delegate;
    [_session startRunning];
}

# pragma mark - Private Methods

- (void)initializeCapture {
    AVCaptureSession *session = [self setupSession];
    if (!session) {return;}
    [self.layer addSublayer:[self setupPreviewLayer]];
    [self setupConnection];
    [self setupOutput];
    [self setupUIGestureRecognizer];
    
    // Start Key Value Obeserver
    [self addObserver:self forKeyPath:@"bounds" options:0 context:nil];

}

- (AVCaptureSession *)setupSession {
    AVCaptureDeviceInput *input = [self setupInput];
    if (!input) {
        return nil;
    }
    
    _session = [AVCaptureSession new];
    [_session addInput:input];
    return _session;
}

- (AVCaptureDeviceInput *)setupInput {
    NSError *error;
    AVCaptureDevice *device = [self cameraWithPosition:self.position];
    return [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
}

- (void)setupOutput {
    self.imageOutput = [[AVCaptureStillImageOutput alloc] init];
    [_session addOutput:self.imageOutput];
}

- (AVCaptureVideoPreviewLayer *)setupPreviewLayer {
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_session];
    [_previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_previewLayer setFrame:self.bounds];
    return _previewLayer;
}

- (AVCaptureConnection *)setupConnection {
    AVCaptureConnection *connection = _previewLayer.connection;
//    connection.videoOrientation = [self getVideoOrientation];
    return connection;
}

- (void)setupUIGestureRecognizer {
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self addGestureRecognizer:recognizer];
    recognizer.delegate = self;
}

- (void)invokeDelegateWithImage:(UIImage *)image {
    [self.delegate incamView:self captureOutput:image];
}

- (void)invokeDelegateWithData:(NSData *)data {
    UIImage *image = [self imageWithData:data];
    [self invokeDelegateWithImage:image];
}

- (UIImage *)imageWithData:(NSData *)data {
    return [UIImage imageWithData:data];
}

- (void)shutterCamera {
    AVCaptureConnection *connection = [[self.imageOutput connections] lastObject];
//    if ([connection isVideoOrientationSupported])
//    {
//        [connection setVideoOrientation:(AVCaptureVideoOrientation) [[UIDevice currentDevice] orientation]];
//    }
    [self.imageOutput captureStillImageAsynchronouslyFromConnection:connection
                                                  completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                                                      NSData *data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                                                      [self invokeDelegateWithData:data];
                                                  }];
}

# pragma mark UIGestureRecognizer

- (void)handleTapFrom:(UITapGestureRecognizer *)recognizer {
    [self shutterCamera];
}


- (void)takePicture {
    [self shutterCamera];
}


- (void)takePictures:(int) count {
    
    _animatedImagesCount = count;
    _animatedImagesIterator = 0;
    
    for (int i = 0; i < self.animatedImagesCount; i++) {
        @autoreleasepool {
            [NSTimer scheduledTimerWithTimeInterval:self.animatedImagesDelay*(i) target:self selector:@selector(captureGifStillImage) userInfo:nil repeats:NO];
            
        }
    }
    
}

- (void) captureGifStillImage
{
    AVCaptureConnection *connection = [[self.imageOutput connections] lastObject];
//    if ([connection isVideoOrientationSupported])
//    {
//        [connection setVideoOrientation:(AVCaptureVideoOrientation) [[UIDevice currentDevice] orientation]];
//    }
    [self frontFlash];
    [self.imageOutput captureStillImageAsynchronouslyFromConnection:connection
                                                  completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                                                      NSData *data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                                                      [self pushImageToGif:[UIImage imageWithData:data]];
                                                  }];

}

- (void) flipCamera
{
    if(self.position == AVCaptureDevicePositionFront) {
        self.position = AVCaptureDevicePositionBack;
    } else {
        self.position = AVCaptureDevicePositionFront;
    }
}
- (AVCaptureDevicePosition) position
{
    if(!_position) {
        // Default camera position is front;
        self.position = AVCaptureDevicePositionFront;
    }
    
    return _position;
}

- (void) setPosition:(AVCaptureDevicePosition) position
{
    _position = position;
    
    //Change camera source
    if(self.session)
    {
        //Indicate that some changes will be made to the session
        [self.session beginConfiguration];
        
        //Remove existing input
        AVCaptureInput* currentCameraInput = [self.session.inputs objectAtIndex:0];
        [self.session removeInput:currentCameraInput];
        
        //Get new input
        AVCaptureDevice *newCamera = [self cameraWithPosition:self.position];
        
        //Add input to session
        NSError *err = nil;
        AVCaptureDeviceInput *newVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:newCamera error:&err];
        if(!newVideoInput || err)
        {
            NSLog(@"Error creating capture device input: %@", err.localizedDescription);
        }
        else
        {
            [self.session addInput:newVideoInput];
        }
        
        //Commit all the configuration changes at once
        [self.session commitConfiguration];
    }

}


// Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
- (AVCaptureDevice *) cameraWithPosition:(AVCaptureDevicePosition) position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position) return device;
    }
    return nil;
}

#pragma mark - Overriding of setters and getters

- (void) setAnimatedImagesCount:(int)animatedImagesCount
{
    _animatedImagesCount = animatedImagesCount;
}
- (int) animatedImagesCount
{
    if (!_animatedImagesCount) {
        _animatedImagesCount = 5;
    }
    return _animatedImagesCount;
}

- (void) setAnimatedImagesDelay:(float)animatedImagesDelay
{
    _animatedImagesDelay = animatedImagesDelay;
}
- (float) animatedImagesDelay
{
    if (_animatedImagesDelay) {
        return _animatedImagesDelay;
    }
    return 0.35;
}


- (void)pushImageToGif:(UIImage *) photo
{
    photo = [photo imageByScalingAndCroppingForSize:(CGSize){1024, 1024}];
    
    NSMutableArray *aux = [self.gifImagesArray mutableCopy];
    [aux addObject:photo];
    _gifImagesArray = [NSArray arrayWithArray:aux];
    
    _animatedImagesIterator++;
    
    if(_animatedImagesIterator >= self.animatedImagesCount) {
        [self.delegate incamViewArray:self captureOutputArray:self.gifImagesArray];
        _animatedImagesIterator = 0;
        self.gifImagesArray = [NSArray array];
    }
}
- (NSArray *) gifImagesArray
{
    if(_gifImagesArray == nil) {
        _gifImagesArray = [NSArray array];
    }
    return _gifImagesArray;
}
- (void) setGifImagesArray:(NSArray *)gifImagesArray
{
    _gifImagesArray = gifImagesArray;
}


-(void)frontFlash
{
    // Attach white backgrounded view to the window
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    whiteView.backgroundColor = [UIColor whiteColor];
    whiteView.alpha = 1.0;
    
    //Animate from alpha 1.0 to 0.0 on 0,5s
    [UIView animateWithDuration: 0.5
                     animations: ^{
                         whiteView.alpha = 0.0;
                     }
                     completion: ^(BOOL finished) {
                         [whiteView removeFromSuperview];
                     }
     ];
    
    //Attach whiteView to current window, on top of all other views.
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    if (!window)
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    [[[window subviews] objectAtIndex:0] addSubview:whiteView];

}

#pragma mark - Value-Key observers
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self && [keyPath isEqualToString:@"bounds"]) {
        // do your stuff, or better schedule to run later using performSelector:withObject:afterDuration:
        if(!_previewLayer) [self setupPreviewLayer];
        [_previewLayer setFrame:self.bounds];
        AVCaptureConnection *connection = _previewLayer.connection;
//        [connection setVideoOrientation:[self getVideoOrientation]];
    }
}
- (int) getVideoOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    AVCaptureVideoOrientation videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
            videoOrientation = AVCaptureVideoOrientationLandscapeRight;
            break;
        case UIDeviceOrientationLandscapeRight:
            videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
            break;
        case UIDeviceOrientationPortrait:
            videoOrientation = AVCaptureVideoOrientationPortrait;
            break;
        default:
            videoOrientation = AVCaptureVideoOrientationPortrait;
            break;
    }
    return videoOrientation;
}
@end
