//
//  ViewController.h
//  GIFme
//
//  Created by Gustavo Peláez on 26/11/15.
//  Copyright © 2015 Gustavo Peláez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KIIncam.h"
#import "ICGNavigationController.h"
#import "ICGViewController.h"


@interface ViewController : ICGViewController <KIIncamDelegate>


@property (nonatomic, getter=isGridShown, setter=showGrid:) BOOL gridShow;

@property (weak,nonatomic) IBOutlet KIIncam *incam;
@property (weak,nonatomic) IBOutlet UIImageView *captureButton;
@property (weak,nonatomic) IBOutlet UIImageView *cameraGrid;
@property (weak,nonatomic) IBOutlet UIView *temporizerLayer;
@property (weak,nonatomic) IBOutlet UILabel *temporizerLabel;


- (IBAction)toggleGrid:(id)sender;
- (IBAction)toggleTemporizer:(id)sender;
- (IBAction)flipCamera:(id)sender;
- (IBAction)takePicture:(id)sender;
- (IBAction)startTemporizer:(id)sender;

@end

